# ZDir

ZDir is a tiny DOS directory lister. ZDir automatically determines how many columns to display, it can display a full tree, the colors are completely configurable, it works in any text mode, and it is lightning fast (written in assembly language).

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## ZDIR.LSM

<table>
<tr><td>title</td><td>ZDir</td></tr>
<tr><td>version</td><td>2.1b</td></tr>
<tr><td>entered&nbsp;date</td><td>2019-07-09</td></tr>
<tr><td>description</td><td>ZDir is a tiny DOS directory lister</td></tr>
<tr><td>summary</td><td>ZDir is a tiny DOS directory lister. ZDir automatically determines how many columns to display, it can display a full tree, the colors are completely configurable, it works in any text mode, and it is lightning fast (written in assembly language).</td></tr>
<tr><td>keywords</td><td>asm, assembler, assembly, a86, dir</td></tr>
<tr><td>author</td><td>Chris Kirmse</td></tr>
<tr><td>primary&nbsp;site</td><td>http://www.tipovers.org/zdir/</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://github.com/ckirmse/ZDir</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, v3</td></tr>
</table>
